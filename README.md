# Thinkphp6 , Migration

### 简介

针对think-migrate创建数据表的功能修改成链式的方式调用

[gitee](https://gitee.com/magein/think-migration) | [composer](https://packagist.org/packages/magein/think-migration) | [lastest version v1.0.2](https://gitee.com/magein/think-migration/tags) | [thinkphp6官方文档](https://www.kancloud.cn/manual/thinkphp6_0/1118028)

### 说明

借鉴了laravel的migrate创建形式，并非碰瓷laravel+think-migration，完全出于个人开发习惯

tp2.x 、tp3.x、tp5.x、tp6.x 一直都有使用，非常喜欢tp框架，因为工作原因也使用laravel

但是偏爱tp

### 依赖

生产环境验证topthink/think-migration的版本

1. 3.1.1

### 解决的问题

1. 主要解决第三个参数拼写的问题，修改成链式调用的方式

```php
$this->addColum('intro','string',['comment'=>'','简介'])
$this->addColum('age','integer',['comment'=>'','年龄'])
```

2. 支持使用下划线的方式创建表

```php
php think migrate:new  user_roles
```

3. 支持composer.extra扩展,安装即用

```json
{
  "extra": {
    "think": {
      "services": [
        "magein\\think\\migration\\service\\CommandService"
      ]
    }
  }
}
```

### 安装

```
composer require magein/think-migration
```

### 使用

#### 可选配置

在config/console.php添加一下配置

```php
'config' => [
    // 数据库迁移工具使用的配置文件
    'migrate' => [
        'mysql' => [
            // 默认引擎
            'engine' => 'InnoDB',
            // 字符集校队规则 .env文件中为charset建议设置为utf8mb4
            // utf8_general_ci、utf8_unicode_ci、utf8mb4_general_ci、utf8mb4_unicode_ci
            'collation' => 'utf8mb4_unicode_ci',
            // 数据库版本 是数据库的版本  而不是php安装的扩展版本  ^v1.x版本废除此参数
            'version' => env('database.version')
        ]
    ],
],
```

#### 创建表

```php
php think migrate:new users
// 增加money、integral字段（仅生成迁移文件）
php think migrate:new users -a money,integral
// 移除email、role_id字段（仅生成迁移文件）
php think migrate:new usuer -r email,role_id
// 更新status、balance字段（仅生成迁移文件）
php think migrate:new users -u status,balance 
```

在database/migrations目录下生成的create_users_table.php文件中输入数据库字段信息

```php

protected $name = 'users';

public function change()
{
    $table = $this->init();
    $table->id();
    $this->create($table);
}

```

添加字段

```php
public function change()
{
    $table = $this->init();
    $table->id();
    $table->uuid('uuid');
    $table->string('username', '登录账号', 30)->unique();
    $table->string('password', '登录密码', 128);
    $table->char('phone', '手机号码', 11)->index();
    $table->string('email', '邮箱地址', 64)->nullable();
    $table->integer('integral', '积分')->default();
    $table->integerTiny('from', '来源 1 注册 2 小程序')->default(1);
    $table->integerSmall('order_total', '订单数量')->default(0);
    $table->integerSmall('age', '年龄')->nullable();
    $table->integerTiny('sex', '性别')->default(0);
    $table->date('birthday', '生日')->nullable();
    $table->dateTime('last_login_time', '最后一次登录时间')->nullable();
    $table->boolean('status')->comment('状态 0 禁用 1 启用')->default(1);
    $table->time('notify')->comment('通知时间')->nullable();

    // 这里调用的$this->create() 而非$table->create()
    $this->create($table);
}

```

#### 表设置

```php
$table = $this->init();
$table->setEngine();
// or
$table->innoDB();
$table->myISAM();

// 设置字符集
$table->setCollation();
// 设置表描述
$table->setComment();

```

#### 设置索引

```php
$table = $this->init();
// 不指定长度默认为32
$table->char('phone', '手机号码')->unique();
$table->char('phone', '手机号码',11)->unique();
$table->char('phone', '手机号码',11)->unique('phone_uniq');
// 或者
$table->addIndex('phone');
$table->addIndex(['phone']);
$table->addIndex(['phone'],'index_phone');
$table->addUniqueKey('phone');
$table->addIndex(['phone'],'unique_phone');
$table->addForeignKey();

// 表创建完成之后，设置索引 请注意这里使用的是$this
$this->addIndex();
$this->addUniqueIndex();
$this->removeIndex();
$this->removeIndexByName();
$this->addForeignKey();
$this->dropForeignKey();

```

#### 常用字段

```php
$table = $this->init();
$table->id();
$table->userId();
$table->memberId();
$table->email();
$table->phone();
$table->remark();
$table->money();
$table->type();
$table->password();
$table->name();
$table->title();
$table->sort();
$table->beginTime();
$table->startTime();
$table->endTime();
$table->status();
```

#### 其他操作

从命名

```php
$table = $this->init();
$table->rename('旧字段名称','新名称');
$table->update();
```

删除字段

> 使用change方法操作，remove将被舍弃,需要使用下面方式

```php
public function up(){
    $table = $this->init();
    $table->remove('字段名称');
    $table->update();
}

public function down(){
    $table = $this->init();
    $table->string('字段名称');
    $table->update();
}
```

删除表

```php
$this->drop();
```

判断表是否存在

```php
$this->exist();
```

获取 Phinx\Db\Table实例

```php
$this->invokeTable();
```

#### 常见错误

索引字段长度设置不正确

1. General error: 1709 Index column size too large. The maximum column size is 767 bytes

检查数据库的charset。如果是utf8mb4则字符集为4个字节，支持索引最大长度为191，在varchar(255上创建索引则会失败)

2. datetime字段引起的错误

datetime的精度(precision参数)在5.6版本之后才支持，所以如果使用的mysql低于此版本，需要在配置文件中配置version参数

### 更新日志

#### v1.0.2

> 2024-07-22

1. 修复字段设置索引字符串长度的无效的问题

#### v1.0.1

> 2024-03-09

1. 修复字段设置索引字符串长度的无效的问题
2. 优化字符默认长度，utf8为255、utf8mb4为191，该问题会导致设置唯一索引的时候出现错误

```php
# 问题：设置唯一索引长度为128，但是创建后是32
$table->string('path', '权限路径',128)->unique();
```

#### v1.0.0

> 2024-02-24

1. 修复通过init得到实例添加索引无效的问题
2. 修改命令空间参数
3. 修改目录首字符为小写

> 2024-01-25

此版本进行了重构，由于之前的版本不支持rollback以及rename、remove、等方法，所以进行了重构，且不支持~v0.x直接升级到此版本

1. 重构Command目录下的Migrate.php
2. src/Stubs目录新增 append、remove模版文件，修改migrate.stub模版文件
3. 重构Column.php、Creator.php文件
4. 新增Migrator.php、Property.php文件
5. 提出配置文件中的version参数，修改为从服务端获取真实的mysql版本
6. 优化thinkphp-migration操作引起的报错信息，如删除字段、删除索引但是这些不存在的问题

#### v0.0.3

1. 将配置文件由config/database.php中的migrations参数移动到config/console.php的config.migrate参数中
2.

新增一个version参数用于配置数据库的版本，此版本是数据库的服务版本而不是php中数据库的扩展版本，如在mysql中使用 `select version()`
查看mysql版本

3. 修改mysql字段类型datetime的精度问题precision引起的错误，datetime精度在5.6版本之后才支持

#### v0.0.2

1. 支持email字段
2. 支持phone字段 char类型 长度为11
3. 支持money字段 int类型 单位是分
4. 支持remark字段