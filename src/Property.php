<?php

namespace magein\think\migration;

use Phinx\Db\Adapter\MysqlAdapter;

class Property
{
    /**
     * 普通索引
     * @var array
     */
    public $index = [];

    /**
     * 唯一索引
     * @var array
     */
    public $unique = [];

    /**
     * 外键
     * @var array
     */
    public $foreign = [];

    /**
     * @var string|float|int
     */
    protected $version = '';

    /**
     *  可选值：
     *   comment     给表结构设置文本注释
     *   row_format  设置行记录模格式
     *   engine      表引擎 (默认 InnoDB)
     *   collation    表字符集
     *   signed       是否无符号 signed(默认 true)
     * @var array
     */
    protected $options = [];

    protected $defaultStringLength = 255;

    /**
     * 添加的列
     * @var array
     */
    protected $columns = [];

    public function __construct($options = null)
    {

        if (is_string($options)) {
            $this->options['comment'] = $options;
        }

        if (!isset($options['id'])) {
            $this->options['id'] = false;
        }

        $database_config = app()->config->get('database');
        $driver = $database_config['default'];
        $charset = $database_config['connections'][$driver]['charset'];

        if ($charset == 'utf8mb4') {
            $this->defaultStringLength = 191;
            $collation = 'utf8mb4_unicode_ci';
        } else {
            $collation = 'utf8_unicode_ci';
        }

        $config = app()->config->get('console.config.migrate.' . $driver);
        if (empty($this->options['engine'])) {
            $this->setEngine(($config['engine'] ?? '') ?: 'InnoDB');
        }

        if (empty($this->options['collation'])) {
            $this->setCollation(($config['collation'] ?? '') ?: $collation);
        }
    }

    /**
     * 设置主键
     * @param $value
     * @return void
     */
    public function setPrimaryKey($value = null)
    {
        if (is_null($value)) {
            $this->options['id'] = false;
            unset($this->options['primary_key']);
        } else {
            $this->options['id'] = true;
            $this->options['primary_key'] = $value;
        }
    }

    /**
     * @param string $engine
     */
    public function setEngine(string $engine)
    {
        $this->options['engine'] = $engine;
    }

    /**
     * 设置表注释
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->options['comment'] = $comment;
    }

    /**
     * 设置排序比对方法
     * @param string $collation
     */
    public function setCollation(string $collation)
    {
        $this->options['collation'] = $collation;
    }

    /**
     * 设置InnoDB引擎
     * @return void
     */
    public function innoDB()
    {
        $this->options['engine'] = 'InnoDB';
    }

    /**
     * 设置MyISAM引擎
     * @return void
     */
    public function myISAM()
    {
        $this->options['engine'] = 'MyISAM';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * 设置版本
     * @param string|float|int $version
     * @return void
     */
    public function setVersion($version)
    {
        if ($version) {
            $this->version = $version;
        }
    }

    /**
     * @return float|int|string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * 获取所有的列
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * 添加一列
     * @param string $type
     * @param string $name
     * @param array $parameters
     * @return Column
     */
    public function addColumn(string $type, string $name, array $parameters = []): Column
    {
        $column = new Column();
        $column->name($name);
        $column->type($type);
        $column->others($parameters);

        if (isset($parameters['null']) && $parameters['null']) {
            $column->nullable();
        }

        if (isset($parameters['length']) && $parameters['length']) {
            $column->length($parameters['length']);
        }

        $this->columns[$name] = $column;

        return $column;
    }

    /**
     * 添加索引或者组合索引
     * @param string|array $columns
     * @param $options
     * @return void
     */
    public function addIndex($columns, $options = null)
    {
        if (!is_array($columns)) {
            $columns = [$columns];
        }

        $options = $this->indexOptions($columns, $options);

        $this->index[] = [$columns, $options];
    }

    /**
     * 添加唯一索引
     * @param string|array $columns
     * @param $options
     * @return void
     */
    public function addUniqueKey($columns, $options = null)
    {
        if (!is_array($columns)) {
            $columns = [$columns];
        }

        $options = $this->indexOptions($columns, $options);
        $options['unique'] = true;

        $this->unique[] = [$columns, $options];
    }

    /**
     * 添加外键索引
     * @param string $columns
     * @param $referenced_table
     * @param $referenced_columns
     * @param array $options
     * @return void
     */
    public function addForeignKey($columns, $referenced_table, $referenced_columns = ['id'], array $options = [])
    {
        if (!is_array($columns)) {
            $columns = [$columns];
        }

        $this->foreign[] = [$columns, $referenced_table, $referenced_columns, $options];
    }

    public function id()
    {
        $this->options['id'] = true;
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function bigIncrements(string $column = 'id', string $comment = null): Column
    {
        return $this->unsignedBigInteger($column, $comment, true);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @return Column
     */
    public function unsignedBigInteger(string $column, string $comment = null, bool $increment = false): Column
    {
        return $this->bigInteger($column, $comment, false, $increment);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @param bool $signed
     * @return Column
     */
    public function bigInteger(string $column, string $comment = null, bool $signed = true, bool $increment = false): Column
    {
        $limit = MysqlAdapter::INT_BIG;

        return $this->addColumn('biginteger', $column, compact('increment', 'signed', 'limit', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @return Column
     */
    public function unsignedInteger(string $column, string $comment = null, bool $increment = false): Column
    {
        return $this->integer($column, $comment, false, $increment);
    }

    /**
     * 添加自增列
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function integerIncrements(string $column = 'id', string $comment = null): Column
    {
        return $this->unsignedInteger($column, $comment, true);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @param bool $signed
     * @return Column
     */
    public function integer(string $column, string $comment = null, bool $signed = true, bool $increment = false): Column
    {
        return $this->addColumn('integer', $column, compact('comment', 'increment', 'signed'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @param bool $signed
     * @return Column
     */
    public function integerTiny(string $column, string $comment = null, bool $signed = true, bool $increment = false): Column
    {
        $limit = MysqlAdapter::INT_TINY;

        return $this->addColumn('integer', $column, compact('comment', 'increment', 'signed', 'limit'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @param bool $signed
     * @return Column
     */
    public function integerSmall(string $column, string $comment = null, bool $signed = true, bool $increment = false): Column
    {
        $limit = MysqlAdapter::INT_SMALL;

        return $this->addColumn('integer', $column, compact('comment', 'increment', 'signed', 'limit'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param bool $increment
     * @param bool $signed
     * @return Column
     */
    public function integerMedium(string $column, string $comment = null, bool $signed = true, bool $increment = false): Column
    {
        $limit = MysqlAdapter::INT_MEDIUM;

        return $this->addColumn('integer', $column, compact('comment', 'increment', 'signed', 'limit'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param $length
     * @return Column
     */
    public function char(string $column, string $comment = null, $length = null): Column
    {
        $length = $length ?: 32;

        return $this->addColumn('char', $column, compact('length', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param $length
     * @return Column
     */
    public function string(string $column, string $comment = null, $length = null): Column
    {
        $length = $length ?: $this->defaultStringLength;

        return $this->addColumn('string', $column, compact('length', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function text(string $column, string $comment = null): Column
    {
        return $this->addColumn('text', $column, compact('comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function textTiny(string $column, string $comment = null): Column
    {
        $limit = MysqlAdapter::TEXT_TINY;

        return $this->addColumn('text', $column, compact('limit', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function textMedium(string $column, string $comment = null): Column
    {
        $limit = MysqlAdapter::TEXT_MEDIUM;

        return $this->addColumn('text', $column, compact('limit', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function textLong(string $column, string $comment = null): Column
    {
        $limit = MysqlAdapter::TEXT_LONG;

        return $this->addColumn('text', $column, compact('limit', 'comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @param int $scale
     * @param bool $signed
     * @return Column
     */
    public function float(string $column, string $comment = null, int $precision = 8, int $scale = 2, bool $signed = false): Column
    {
        return $this->addColumn('float', $column, compact('comment', 'precision', 'scale', 'signed'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @param int $scale
     * @return Column
     */
    public function floatUnsigned(string $column, string $comment = null, int $precision = 8, int $scale = 2): Column
    {
        return $this->float($column, $comment, $precision, $scale, true);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param integer|null $precision
     * @param integer|null $scale
     * @param bool $signed
     * @return Column
     */
    public function double(string $column, string $comment = null, int $precision = null, int $scale = null, bool $signed = false): Column
    {
        return $this->addColumn('double', $column, compact('comment', 'precision', 'scale', 'signed'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param integer|null $precision
     * @param integer|null $scale
     * @return Column
     */
    public function doubleUnsigned(string $column, string $comment = null, int $precision = null, int $scale = null): Column
    {
        return $this->double($column, $comment, $precision, $scale, true);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param integer $precision
     * @param integer $scale
     * @param bool $signed
     * @return Column
     */
    public function decimal(string $column, string $comment = null, int $precision = 10, int $scale = 2, bool $signed = false): Column
    {
        return $this->addColumn('decimal', $column, compact('comment', 'precision', 'scale', 'signed'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @param int $scale
     * @return Column
     */
    public function decimalUnsigned(string $column, string $comment = null, int $precision = 10, int $scale = 2): Column
    {
        return $this->decimal($column, $comment, $precision, $scale, true);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function boolean(string $column, string $comment = null): Column
    {
        return $this->addColumn('boolean', $column, compact('comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function date(string $column, string $comment = null): Column
    {
        return $this->addColumn('date', $column, compact('comment'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @param bool $null
     * @return Column
     */
    public function dateTime(string $column, string $comment = null, int $precision = 0, bool $null = false): Column
    {
        if ($this->version < 5.6) {
            $options = compact('comment', 'null');
        } else {
            $options = compact('comment', 'precision', 'null');
        }

        return $this->addColumn('datetime', $column, $options);
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @return Column
     */
    public function time(string $column, string $comment = null, int $precision = 0): Column
    {
        return $this->addColumn('time', $column, compact('comment', 'precision'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @param int $precision
     * @return Column
     */
    public function timestamp(string $column, string $comment = null, int $precision = 0): Column
    {
        return $this->addColumn('timestamp', $column, compact('comment', 'precision'));
    }

    /**
     * @param string $column
     * @param string|null $comment
     * @return Column
     */
    public function uuid(string $column, string $comment = null): Column
    {
        return $this->addColumn('uuid', $column, compact('comment'));
    }

    /**
     * @param int $precision
     * @return Column
     */
    public function createdAt(int $precision = 0): Column
    {
        return $this->dateTime('created_at', '创建时间', $precision);
    }

    /**
     * @param int $precision
     * @return Column
     */
    public function updatedAt(int $precision = 0): Column
    {
        return $this->dateTime('updated_at', '更新时间', $precision);
    }

    /**
     * @param int $precision
     * @return Column
     */
    public function deletedAt(int $precision = 0): Column
    {
        return $this->dateTime('deleted_at', '删除时间', $precision, true);
    }

    /**
     * @param $comment
     * @param string|int $default
     * @return Column
     */
    public function status($comment = null, $default = 1): Column
    {
        $comment = $comment ?: '状态 0 禁用 forbid 1 启用 open';

        return $this->integerTiny('status', $comment)->default($default);
    }

    /**
     * 排序字段
     * @param $comment
     * @param string|int $default
     * @return Column
     */
    public function sort($comment = null, $default = 99): Column
    {
        $comment = $comment ?: '排序 正序';

        return $this->integerSmall('sort', $comment)->default($default);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function phone($comment = null): Column
    {
        $comment = $comment ?: '手机号码';

        return $this->char('phone', $comment, 11);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function email($comment = null): Column
    {
        $comment = $comment ?: '邮箱地址';

        return $this->string('email', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function password($comment = null): Column
    {
        $comment = $comment ?: '密码';

        return $this->string('password', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function name($comment = null): Column
    {
        $comment = $comment ?: '名称';

        return $this->string('name', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function title($comment = null): Column
    {
        $comment = $comment ?: '标题';

        return $this->string('title', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function money($comment = null): Column
    {
        $comment = $comment ?: '金额 单位分';

        return $this->integer('money', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function remark($comment = null): Column
    {
        $comment = $comment ?: '备注';

        return $this->string('remark', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function userId($comment = null): Column
    {
        $comment = $comment ?: '用户id';

        return $this->unsignedInteger('user_id', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function memberId($comment = null): Column
    {
        $comment = $comment ?: '用户id';

        return $this->unsignedInteger('member_id', $comment);
    }

    /**
     * @param $comment
     * @param string|int $default
     * @return Column
     */
    public function type($comment = null, $default = 0): Column
    {
        $comment = $comment ?: '类型';

        return $this->integerTiny('type', $comment)->default($default);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function beginTime($comment = null): Column
    {
        $comment = $comment ?: '开始时间';

        return $this->dateTime('begin_time', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function startTime($comment = null): Column
    {
        $comment = $comment ?: '开始时间';

        return $this->dateTime('start_time', $comment);
    }

    /**
     * @param $comment
     * @return Column
     */
    public function endTime($comment = null): Column
    {
        $comment = $comment ?: '结束时间';

        return $this->dateTime('end_time', $comment);
    }

    /**
     * 从命名
     * @param string $name
     * @param string $new_name
     * @return Column
     */
    public function rename(string $name, string $new_name): Column
    {
        $column = new Column();
        $column->name($name);
        $column->rename($name, $new_name);
        $this->columns[$name] = $column;
        return $column;
    }

    /**
     * @param string $name
     * @param $type
     * @param string|null $comment
     * @return Column
     */
    public function change(string $name, $type, string $comment = null): Column
    {
        $column = new Column();
        $column->name($name);
        $column->type($type);
        $column->comment($comment ?: '');
        $column->change();
        $this->columns[$name] = $column;
        return $column;
    }

    /**
     * 请不要再change方法中调用，如果在change方法中调用，将被舍弃
     * @param $name
     * @return Column
     */
    public function remove($name): Column
    {
        $column = new Column();
        $column->name($name);
        $column->remove();
        $this->columns[$name] = $column;
        return $column;
    }

    /**
     * @param string|array $columns
     * @param $options
     * @return array
     */
    protected function indexOptions($columns, $options = null): array
    {
        if (!is_array($columns)) {
            $columns = [$columns];
        }

        if (is_null($options)) {
            $options = [];
        } elseif (is_string($options)) {
            $name = $options;
            $options = [];
            $options['name'] = $name;
        }

        if (empty($options['name'] ?? '')) {
            $options['name'] = implode('_', $columns);
        }

        return $options;
    }
}