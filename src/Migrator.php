<?php

/**
 * @user magein
 * @date 2024/1/25 14:07
 */

namespace magein\think\migration;

use magein\utils\Variable;
use Phinx\Db\Table;

class Migrator extends \think\migration\Migrator
{
    /**
     * @var Property|null
     */
    protected $property = null;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @param $message
     * @return void
     */
    protected function exception($message)
    {
        echo "文件 : " . Variable::ins()->underline(static::class);
        echo "\n";
        echo $message;
        exit(1);
    }

    public function init($options = null): Property
    {
        $property = new Property($options);

        $version = $this->fetchRow('select version();');

        if ($version[0] ?? '') {
            $version = $version[0];
            $property->setVersion($version);
        }

        return $property;
    }

    /**
     * @return Table
     */
    public function invokeTable(): Table
    {
        $name = $this->name;
        if (empty($name)) {
            $this->exception('请设置表名称');
        }

        $options = [];
        if ($this->property) {
            $options = $this->property->getOptions();
        }

        return $this->table($name, $options);
    }

    /**
     * @param Property $Property
     * @return $this
     */
    public function setProperty(Property $Property): Migrator
    {
        $this->property = $Property;

        return $this;
    }

    /**
     * @param Table $table
     * @return Table
     */
    protected function parse(Table $table): Table
    {
        $columns = $this->property->getColumns();

        foreach ($columns as $column) {
            /**
             * @var Column $column
             */
            $name = $column->getName();
            $type = $column->getType();

            $options = $column->getOthers();

            if ($column->getDefault() !== -1) {
                $options['default'] = $column->getDefault();
            }

            if ($column->getLength()) {
                $options['limit'] = $column->getLength();
            }

            if ($column->getAfter()) {
                $options['after'] = $column->getAfter();
            }

            if ($column->getComment()) {
                $options['comment'] = $column->getComment();
            }

            $options['null'] = $column->getNull();
            if ($column->getNull()) {
                unset($options['default']);
            }

            if ($column->isRemove() && $this->isMigratingUp()) {
                if (!method_exists(static::class, 'change')) {
                    $table->removeColumn($name);
                }
            } elseif ($rename = $column->isRename()) {
                if (count($rename) == 2) {
                    $table->renameColumn($rename[0], $rename[1]);
                }
            } elseif ($column->isChange()) {
                $table->changeColumn($name, $type, $options);
            } else {
                $table->addColumn($name, $type, $options);

                if ($column->getUnique()) {
                    $table->addIndex($column->getUnique(),
                        [
                            'name' => $column->getUnique(),
                            'unique' => true,
                        ]
                    );
                } elseif ($column->getIndex()) {
                    $table->addIndex($column->getIndex(), [
                        'name' => $column->getIndex(),
                    ]);
                }
            }
        }

        if ($this->property->unique) {
            foreach ($this->property->unique as $item) {
                $table->addIndex($item[0], $item[1] ?? []);
            }
        } elseif ($this->property->index) {
            foreach ($this->property->index as $item) {
                $table->addIndex($item[0], $item[1] ?? []);
            }
        }

        return $table;
    }

    /**
     * 执行create方法
     * @param Property $property
     * @param bool $soft_delete
     * @return void
     */
    public function create(Property $property, bool $soft_delete = true)
    {
        $property->createdAt();
        $property->updatedAt();

        if ($soft_delete) {
            $property->deletedAt();
        }

        $this->setProperty($property);
        $this->parse($this->invokeTable())->create();
    }

    /**
     * 执行update方法
     * @param Property $property
     * @return void
     */
    public function update(Property $property)
    {
        $this->setProperty($property);
        $this->parse($this->invokeTable())->update();
    }

    /**
     * 执行save方法
     * @param Property $property
     * @return void
     */
    public function save(Property $property)
    {
        $this->setProperty($property);
        $this->parse($this->invokeTable())->save();
    }

    /**
     * 表是否存在
     * @return bool
     */
    public function exist(): bool
    {
        return $this->hasTable($this->name);
    }

    /**
     * 删除表
     * @return void
     */
    public function drop()
    {
        if ($this->hasTable($this->name)) {
            $this->invokeTable()->drop()->save();
        }
    }

    /**
     * 添加索引
     * @param array|string $columns
     * @param array $options
     * @return void
     */
    public function addIndex($columns, array $options = [])
    {
        $this->invokeTable()->addIndex($columns, $options)->save();
    }

    /**
     * 添加唯一措施一
     * @param array|string $columns
     * @param string|null $name
     * @return void
     */
    public function addUniqueIndex($columns, string $name = null)
    {
        $this->invokeTable()->addIndex($columns, [
            'unique' => true,
            'name' => $name ?: (is_array($columns) ? implode('_', $columns) : $columns)
        ])->save();
    }

    /**
     * 删除索引根据字段
     * @param string|string[] $columns
     * @return void
     */
    public function removeIndex($columns)
    {
        if ($this->invokeTable()->hasIndex($columns)) {
            $this->invokeTable()->removeIndex($columns)->save();
        }
    }

    /**
     * 删除索引根据索引名称
     * @param string $name
     * @return void
     */
    public function removeIndexByName(string $name)
    {
        if ($this->invokeTable()->hasIndexByName($name)) {
            $this->invokeTable()->removeIndexByName($name)->save();
        }
    }

    /**
     * 添加外键
     * @param $columns
     * @param $referencedTable
     * @param string|string[] $referencedColumns
     * @param array $options
     * @return void
     */
    public function addForeignKey($columns, $referencedTable, $referencedColumns = ['id'], array $options = [])
    {
        $this->invokeTable()->addForeignKey($columns, $referencedTable, $referencedColumns, $options)->save();
    }

    /**
     * 删除外键
     * @param $columns
     * @param string|null $constraint
     * @return void
     */
    public function dropForeignKey($columns, ?string $constraint = null)
    {
        if ($this->invokeTable()->hasForeignKey($columns)) {
            $this->invokeTable()->dropForeignKey($columns, $constraint)->save();
        }
    }
}