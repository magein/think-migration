<?php
declare (strict_types=1);

namespace magein\think\migration\service;

use magein\think\migration\command\Migrate;
use think\Service;

class CommandService extends Service
{
    /**
     * 注册服务
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * 执行服务
     *
     * @return void
     */
    public function boot()
    {
        // 添加命令
        $this->commands(
            [
                'migrate:new' => Migrate::class,
            ]
        );
    }
}