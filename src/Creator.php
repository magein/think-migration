<?php

namespace magein\think\migration;

use InvalidArgumentException;
use magein\utils\Variable;
use Phinx\Util\Util;
use RuntimeException;
use think\App;

class Creator
{

    protected $app;

    public $operate = '';

    public $fields = [];

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function create(string $className, $table_name)
    {
        $path = $this->ensureDirectory();

        if (!Util::isValidPhinxClassName($className)) {
            throw new InvalidArgumentException(sprintf('The migration class name "%s" is invalid. Please use CamelCase format.', $className));
        }

        if (!Util::isUniqueMigrationClassName($className, $path)) {
            throw new InvalidArgumentException(sprintf('The migration class name "%s" already exists', $className));
        }

        // Compute the file path
        $fileName = Util::mapClassNameToFileName($className);
        $filePath = $path . DIRECTORY_SEPARATOR . $fileName;

        if (is_file($filePath)) {
            throw new InvalidArgumentException(sprintf('The file "%s" already exists', $filePath));
        }

        // Verify that the template creation class (or the aliased class) exists and that it implements the required interface.
        $aliasedClassName = null;

        // Load the alternative template if it is defined.
        if ($this->operate == 'remove') {
            $contents = $this->removeTemplate();
        } elseif ($this->operate == 'append' || $this->operate == 'update') {
            $contents = $this->appendTemplate();
        } else {
            $contents = file_get_contents($this->getTemplate());
        }

        $table_name = Variable::ins()->underline($table_name);
        $contents = str_replace('%table_name%', $table_name, $contents);

        // inject the class names appropriate to this migration
        $contents = strtr($contents, [
            'MigratorClass' => $className,
        ]);

        if (false === file_put_contents($filePath, $contents)) {
            throw new RuntimeException(sprintf('The file "%s" could not be written to', $path));
        }

        return $filePath;
    }

    protected function ensureDirectory()
    {
        $path = $this->app->getRootPath() . 'database' . DIRECTORY_SEPARATOR . 'migrations';

        if (!is_dir($path) && !mkdir($path, 0755, true)) {
            throw new InvalidArgumentException(sprintf('directory "%s" does not exist', $path));
        }

        if (!is_writable($path)) {
            throw new InvalidArgumentException(sprintf('directory "%s" is not writable', $path));
        }

        return $path;
    }

    /**
     * @return string
     */
    protected function getTemplate(): string
    {
        return __DIR__ . '/stubs/migrate.stub';
    }

    protected function removeTemplate()
    {
        $content = file_get_contents(__DIR__ . '/stubs/remove.stub');

        $remove = '';
        $append = '';
        if ($this->fields) {

            $fields = $this->fields;
            if (!is_array($fields)) {
                $fields = explode(',', $fields);
            }

            foreach ($fields as $field) {
                $remove .= '        $table->remove(\'' . $field . '\');' . "\n";
                $append .= '        $table->string(\'' . $field . '\');' . "\n";
            }
        }

        $content = str_replace('%append%', $append, $content);

        return str_replace('%remove%', $remove, $content);
    }

    public function appendTemplate()
    {
        $content = file_get_contents(__DIR__ . '/stubs/append.stub');

        $append = '';
        if ($this->fields) {

            $fields = $this->fields;
            if (!is_array($fields)) {
                $fields = explode(',', $fields);
            }
            foreach ($fields as $field) {
                $append .= '        $table->string(\'' . $field . '\');' . "\n";
            }
        }

        return str_replace('%append%', $append, $content);
    }

    /**
     * @param $className
     * @return bool
     */
    public function exist($className): bool
    {
        $path = $this->ensureDirectory();

        return !Util::isUniqueMigrationClassName($className, $path);
    }
}