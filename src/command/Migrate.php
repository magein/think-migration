<?php
declare (strict_types=1);

namespace magein\think\migration\command;

use magein\utils\Variable;
use magein\think\migration\Creator;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument as InputArgument;
use think\console\input\Option;
use think\console\Output;

class Migrate extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('migrate:new')
            ->setDescription('Create a new migration')
            ->addArgument('name', InputArgument::REQUIRED, '这次迁移的名称是什么？?')
            ->addOption('append', 'a', Option::VALUE_REQUIRED, '添加表字段')
            ->addOption('remove', 'r', Option::VALUE_REQUIRED, '移除表字段')
            ->addOption('update', 'u', Option::VALUE_REQUIRED, '更新表字段')
            ->setHelp(sprintf('%sCreates a new database migration%s', PHP_EOL, PHP_EOL));
    }

    protected function execute(Input $input, Output $output)
    {
        $table_name = $input->getArgument('name');
        $append = $input->getOption('append');
        $remove = $input->getOption('remove');
        $update = $input->getOption('update');

        $concat_name = function ($data, $prefix) use ($table_name) {
            $data = explode(',', $data);
            $class_name = $prefix . '_' . $table_name;
            foreach ($data as $item) {
                $class_name .= Variable::ins()->pascal(str_replace('_', '', $item));
            }
            $class_name .= 'Field';
            return $class_name;
        };

        $operate = '';
        $fields = '';
        if ($append) {
            $operate = 'append';
            $fields = $append;
            $class_name = $concat_name($append, $operate);
        } elseif ($remove) {
            $operate = 'remove';
            $fields = $remove;
            $class_name = $concat_name($remove, $operate);
        } elseif ($update) {
            $operate = 'update';
            $fields = $update;
            $class_name = $concat_name($update, $operate);
        } else {
            $class_name = 'create_' . $table_name . '_table';
            $class_name = Variable::ins()->pascal($class_name);
        }

        $class_name = Variable::ins()->pascal($class_name);

        /**
         * @var  Creator $creator
         */
        $creator = invoke(Creator::class);

        // 文件存在，则验证开头是否以append update开头
        if ($creator->exist($class_name) && empty($append) && empty($remove) && empty($update)) {
            $output->writeln(sprintf('%s 已经存在', $class_name));
            $output->writeln('  使用 -a 增加字段参数 多个使用逗号隔开');
            $output->writeln('  使用 -r 删除字段参数 多个使用逗号隔开');
            $output->writeln('  使用 -u 更新字段参数 多个使用逗号隔开');
            exit(1);
        }

        $creator->operate = $operate;
        $creator->fields = $fields;
        $path = $creator->create($class_name, $table_name);

        $output->writeln('<info>created</info> .' . str_replace(getcwd(), '', realpath($path)));
    }
}